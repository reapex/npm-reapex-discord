const Discord = require("discord.js")
const mysql = require('mysql');
const bot = new Discord.Client();
const colors = require('colors');

function mysqlConnect(hostName, userName, passwordName, databaseName){
    var sql = mysql.createConnection({
        host: hostName,
        user: userName,
        password: passwordName,
        database: databaseName
    });

    sql.connect(function (err) {
        if (err) throw err;
        console.log(`RPX MySQL:`.green.bold + ` Successfully connected !`.grey);
    });
}

function setPresence(presenceText, presenceType, presenceStatus){

    bot.user.setPresence({
        status: presenceStatus,
        game: {
            name: presenceText,
            type: presenceType
        }
    });
}

function start(token){
    bot.login(token);
}

function exit(){
    bot.destroy()
}

module.exports = {bot, start, exit, setPresence, mysqlConnect}