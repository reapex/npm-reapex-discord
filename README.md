**Installation:**

``npm i reapex-discord``

**Basic configuration:**

```js
//Importing package as "rpx"
const rpx = require("reapex-discord");
//Creating your bot client
const bot = rpx.bot
//Import mysql
const mysql = rpx.mysql
//Create prefix
var prefix = "."
 
//Simple MySql Config
var sql = mysql.createConnection({
    host: "",
    user: "",
    password: "",
    database: ""
});
```

**Start the bot client:**

```js
// Starting bot
rpx.start('token')
```

**Check if bot client & MySQL can connects:**

```js
//Set presence, can be 'available', 'idle', 'dnd', or 'invisible'
rpx.setPresence('Sample Text', 'WATCHING', 'idle')
//Check your database connection
rpx.mysqlCheck(sql)
```

You can disconnect your bot client with :
```js
rpx.exit()
```
**MySQL queries:**

```js
//Run a simple query
rpx.mysqlQuery(sql, "SHOW TABLES") //Log result 
 
//Retrieve the data
let query = "Your query"
sql.query(query, function (err, result) {
    if (err) {
        console.log(err)
    }
    else {
        console.log(result)
    }
});
```

**Encryption/Decryption:**

Maybe who going to crypt a plain string or decode it ? Use these methods:

Encode your plain text:

```js
rpx.encode('your_plain_text')
```

Decode your encrypted text:

```js
rpx.decode('your_encrypted_text')
```
